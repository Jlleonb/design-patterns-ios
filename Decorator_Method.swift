protocol Character {
    func getHealth() -> Int
}

struct Orc: Character {
    func getHealth() -> Int {
        return 10
    }
}

struct Elf: Character {
    func getHealth() -> Int {
        return 5
    }
}

protocol CharacterType: Character {
    var base: Character { get }
}

struct Warlord: CharacterType {

    let base: Character

    func getHealth() -> Int {
        return 50 + base.getHealth()
    }

    func battleCry() {
        print("RAWR")
    }
}

struct Epic: CharacterType {

    let base: Character

    func getHealth() -> Int {
        return 30 + base.getHealth()
    }
}

let orc = Orc()
let hp = orc.getHealth()
let orcWarlord = Warlord(base: orc)
let hp = orcWarlord.getHealth()
let epicOrcWarlord = Epic(base: orcWarlord)
let hp = epicOrcWarlord.getHealth()
let doubleEpicOrcWarlord = Epic(base: epicOrcWarlord)
let hp = doubleEpicOrcWarlord.getHealth()
let elf = Elf()
let hp = elf.getHealth()
let elfWarlord = Warlord(base: elf)
let hp = elfWarlord.getHealth()
elfWarlord.battleCry()
