protocol InterfaceSuscriber {
  func update(_ announcement:String)
}

class Publisher {
  var suscribers:[Suscriber] = []

  func suscribe(_ suscriber:Suscriber){
    suscribers.append(suscriber)
  }

  func unsuscribe(_ suscriber:Suscriber){
    suscribers.removeLast()
  }

  func makeAnnouncement(_ announcement:String){
    for s in suscribers {
      s.update(announcement)
    }
  }

}

class NewProduct : Publisher {
  override func suscribe(_ suscriber:Suscriber){
    super.suscribe(suscriber)
  }

  override func unsuscribe(_ suscriber:Suscriber){
    super.unsuscribe(suscriber)
  }

  override func makeAnnouncement(_ announcement:String){
    for s in suscribers {
      s.update("The new product is: \(announcement) \n")
    }
  }
}

class NewFood : Publisher {
  override func suscribe(_ suscriber:Suscriber){
    super.suscribe(suscriber)
  }

  override func unsuscribe(_ suscriber:Suscriber){
    super.unsuscribe(suscriber)
  }

  override func makeAnnouncement(_ announcement:String){
    for s in suscribers {
      s.update("The new food is: \(announcement) \n")
    }
  }
}

class Suscriber : InterfaceSuscriber {
  private var name:String

  init (_ name:String){
    self.name = name
  }

  func update(_ announcement:String){
    print ("My name is: ", name, ". ", announcement)
  }
}

var suscriber1 = Suscriber("Pedro")
var suscriber2 = Suscriber("Ana")
var suscriber3 = Suscriber("Jose")
var publisher1 = NewProduct()
var publisher = NewFood()

publisher1.suscribe(suscriber1)
publisher1.suscribe(suscriber3)
publisher.suscribe(suscriber2)
publisher1.makeAnnouncement("Pencil")
publisher.makeAnnouncement("Hamburguer")

print("\n\n")

publisher1.unsuscribe(suscriber3)
publisher.suscribe(suscriber3)

publisher1.makeAnnouncement("Mouse")
publisher.makeAnnouncement("Pizza")
