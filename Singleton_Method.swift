import Foundation

final class Time {
  static let instance = Time()

  private init(){}

  func getTime() -> Date {
      return Date()
  }
}

print("Time is: \(Time.instance.getTime())")
