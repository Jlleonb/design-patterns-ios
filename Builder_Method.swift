protocol Builder {
  func setNumberSpaces(_ spaces:Int);
  func setNumberFloors(_ floor:Int)
  func setNumberEntrance(_ entrances:Int)
  func setNumberExit(_ exits:Int)
  func setCostPerHour(_ price:Double)
}

class ConcreteBuilder: Builder {
  private var parkingLot = ParkingLot()

   func reset(){
     parkingLot = ParkingLot()
   }

   func setNumberSpaces(_ spaces:Int){
     parkingLot.setSpaces(spaces)
   }

   func setNumberFloors (_ floor:Int){
     parkingLot.setFloors(floor)
   }

   func setNumberEntrance (_ entrances:Int){
     parkingLot.setEntrances(entrances)
   }

   func setNumberExit (_ exits:Int){
     parkingLot.setExits(exits)
   }

   func setCostPerHour (_ price:Double){
     parkingLot.setCostperHour(price)
   }

   func retrieveParkingLot() -> ParkingLot {
        let result = self.parkingLot
        reset()
        return result
    }

}

class ParkingLot {
  private var spaces:Int = 25
  private var floors:Int = 1
  private var entrances:Int = 1
  private var exits:Int = 1
  private var costPerHour:Double = 1.0

  init (){

  }

  func setSpaces(_ spaces:Int){
    self.spaces = spaces
  }

  func setFloors(_ floors:Int){
    self.floors = floors
  }

  func setEntrances (_ entrances:Int){
    self.entrances = entrances
  }

  func setExits (_ exits:Int){
    self.exits = exits
  }

  func setCostperHour (_ price:Double){
    self.costPerHour = price
  }

  func showParkingLot () -> String {
    var result = "Parking Lot: \nNumber of Spaces: \(self.spaces) \nNumber of Floors: \(self.floors) \nNumber of Entrances: \(self.entrances) \nNumber of exits: \(self.exits) \nCost per Hour: \(self.costPerHour) \n"
    return result
  }

}

let builder = ConcreteBuilder()
print ("\nNew Parking Lot 1")
builder.setNumberSpaces(50)
builder.setNumberFloors(2)
builder.setNumberEntrance(3)
builder.setNumberExit(3)
builder.setCostPerHour(1.50)
print (builder.retrieveParkingLot().showParkingLot())
print ("\nNew Parking Lot 2\n")
builder.setNumberSpaces(1000)
builder.setCostPerHour(2.00)
print (builder.retrieveParkingLot().showParkingLot())
