class Taste {
  func itTasteLike(){}
}

class OnionTaste:Taste {
  override func itTasteLike(){
    print("It tastes like Onion!")
  }
}

class CheeseTaste:Taste {
  override func itTasteLike(){
    print("It tastes like Cheese!")
  }
}

class Bread {
  private var taste:Taste

  init(_ taste:Taste){
    self.taste = taste
  }

  func typeAndTaste(){
    print("It's a bread!")
    taste.itTasteLike()
  }
}

class Patty {

  private var taste:Taste

  init (_ taste:Taste){
    self.taste = taste
  }

  func typeAndTaste(){
    print("It's a Patty!")
    taste.itTasteLike()
  }
}

var onion:Taste = OnionTaste()
var cheese:Taste = CheeseTaste()

var cheesePatty:Patty = Patty(cheese)
cheesePatty.typeAndTaste()

var onionBread:Bread = Bread(onion)
onionBread.typeAndTaste()
