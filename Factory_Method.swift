protocol Personal {
    func sign()
    func writeReport()
    func readStatement()
}

class Employee : Personal {

    var firstName:String
    var lastName:String
    var department:String
    var salary:Double

    init(_ firstName:String, _ lastName:String, _ department:String,_ salary:Double){
        self.firstName = firstName
        self.lastName = lastName
        self.department = department
        self.salary = salary
    }

    func sign(){
      print("Sign ", firstName, " ", lastName)
    }

    func writeReport(){
      print("Report: ", department, "\n", "Name: ", firstName, " ", lastName)
    }

    func readStatement(){
      print ("Statement: Hello World!")
    }
}

class Boss : Personal {
    var firstName:String
    var lastName:String
    var department:String
    var salary:Double

    init(_ firstName:String, _ lastName:String, _ department:String,_ salary:Double){
        self.firstName = firstName
        self.lastName = lastName
        self.department = department
        self.salary = salary
      }

      func sign(){
        print("Sign ", firstName, " ", lastName)
      }

      func writeReport(){
        print("Report: ", department, "\n", "Name: ", firstName, " ", lastName)
      }

      func readStatement(){
        print ("Statement: I'm the boss")
      }
}

enum PersonalTypes {
  case Boss
  case Employee
}

class PersonalFactory {
  private static var sharedPersonalFactory = PersonalFactory()

  class func shared() -> PersonalFactory {
    return sharedPersonalFactory
  }

  func getPersonal (PersonalType personalType : PersonalTypes, firstName:String, lastName:String, department:String, salary:Double) -> Personal {
    switch personalType {
      case .Employee:
          return Employee(firstName, lastName, department, salary)
      case .Boss:
        return Boss(firstName, lastName, department, salary)
    }
  }

}

let boss = PersonalFactory.shared().getPersonal(PersonalType: .Boss, firstName:"Jose",lastName:"Leon",department:"IT",salary:1000000)
boss.readStatement()

let employee = PersonalFactory.shared().getPersonal(PersonalType: .Employee, firstName:"Jose",lastName:"Murillo",department:"Administrative",salary:2000)
employee.readStatement()
